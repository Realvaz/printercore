package tables;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import entities.Client;
import files.ClientFileController;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import utils.dbConnection;

public class ClientsController implements Initializable {
	
	@FXML
	VBox tableContainer;
	@FXML
	TextField filter;
	@FXML
	Label total, filtered, selected;
	@FXML
	ContextMenu contextMenu;

	TableView<Client> clientsTable = new TableView<Client>();
	
	private int count;
	
	private static StringProperty screenRequired = new SimpleStringProperty();
	public static StringProperty screenRequiredProperty() {
        return screenRequired;
    }
    private void screenRequired(String screen) {
    	screenRequiredProperty().set(screen);
    }
    
	@SuppressWarnings("unchecked")
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		count = 0;

		TableColumn<Client, String> IDCol = new TableColumn<>("ID");
		IDCol.setCellValueFactory(new PropertyValueFactory<>("ID"));
		TableColumn<Client, String> nameCol = new TableColumn<>("Nombre");
		nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
		TableColumn<Client, String> addressCol = new TableColumn<>("Direcci�n");
		addressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
		TableColumn<Client, String> townCol = new TableColumn<>("Poblaci�n");
		townCol.setCellValueFactory(new PropertyValueFactory<>("town"));
		TableColumn<Client, String> phoneNumberCol = new TableColumn<>("Tel�fono");
		phoneNumberCol.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
		TableColumn<Client, String> contactCol = new TableColumn<>("Contacto");
		contactCol.setCellValueFactory(new PropertyValueFactory<>("contact"));
		TableColumn<Client, String> registrationCol = new TableColumn<>("Alta");
		registrationCol.setCellValueFactory(new PropertyValueFactory<>("registration"));
		TableColumn<Client, String> categoryCol = new TableColumn<>("Categor�a");
		categoryCol.setCellValueFactory(new PropertyValueFactory<>("category"));
		TableColumn<Client, String> actionCol = new TableColumn<>("Acciones");
		actionCol.setCellValueFactory(new PropertyValueFactory<>(""));

		Callback<TableColumn<Client, String>, TableCell<Client, String>> cellFactory = new Callback<TableColumn<Client, String>, TableCell<Client, String>>() {
			@Override
			public TableCell<Client, String> call(final TableColumn<Client, String> param) {
				final TableCell<Client, String> cell = new TableCell<Client, String>() {
					@Override
					public void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							JFXButton fileButton = new JFXButton();
							fileButton.getStyleClass().add("success");
							fileButton.setOnAction(event -> screenRequired(getTableView().getItems().get(getIndex()).getName()));						
							FontAwesomeIconView fileIcon = new FontAwesomeIconView();
							fileIcon.setIcon(FontAwesomeIcon.USER);
							fileIcon.setStyle("-fx-fill: #4AA84E;");
							fileIcon.setSize("15");
							fileButton.setGraphic(fileIcon);

							JFXButton deleteButton = new JFXButton();
							deleteButton.getStyleClass().add("error");
							deleteButton.setOnAction(event -> delete(getTableView().getItems().get(getIndex()).getID(), getIndex()));
							FontAwesomeIconView deleteIcon = new FontAwesomeIconView();
							deleteIcon.setIcon(FontAwesomeIcon.CLOSE);
							deleteIcon.setStyle("-fx-fill: #F44336;");
							deleteIcon.setSize("15");
							deleteButton.setGraphic(deleteIcon);

							HBox hb = new HBox();
							hb.getChildren().addAll(fileButton, deleteButton);
							hb.setAlignment(Pos.CENTER);

							setGraphic(hb);
						}
						setText(null);
					}
				};
				return cell;
			}
		};
		actionCol.setCellFactory(cellFactory);

		clientsTable = new TableView<>();
		clientsTable.getColumns().addAll(IDCol, nameCol, addressCol, townCol, phoneNumberCol, contactCol,
				registrationCol, categoryCol, actionCol);

		tableContainer.getChildren().addAll(clientsTable);
		VBox.setVgrow(clientsTable, Priority.ALWAYS);
		
		clientsTable.setRowFactory(tv -> {
			TableRow<Client> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				ObservableList<Client> selected;
				selected = clientsTable.getSelectionModel().getSelectedItems();
				this.selected.setText(selected.size() + " Seleccionados");

				if (event.getClickCount() == 2 && (!row.isEmpty()))
					file();
			});
			return row;
		});

		clientsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		clientsTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		clientsTable.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.isSecondaryButtonDown())
					contextMenu.show(clientsTable, event.getScreenX(), event.getScreenY());
			}
		});

		refreshTable();
		
		ClientFileController.screenRequiredProperty().addListener((obs, oldScreenRequired, newScreenRequired) -> {
			if (newScreenRequired != "") {
				ClientFileController.screenRequiredProperty().set("");
				screenRequired("Clients");
				refreshTable();
			}
		});
	}

	public void refreshTable() {
		clientsTable.setItems(getClients());

		filter.setText("");

		ObservableList<Client> data = clientsTable.getItems();
		filter.textProperty()
				.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
					if (oldValue != null && (newValue.length() < oldValue.length()))
						clientsTable.setItems(data);

					String value = newValue.toLowerCase();
					ObservableList<Client> subentries = FXCollections.observableArrayList();

					long count = clientsTable.getColumns().stream().count();
					for (int i = 0; i < clientsTable.getItems().size(); i++) {
						for (int j = 0; j < count; j++) {
							String entry = "" + clientsTable.getColumns().get(j).getCellData(i);
							if (entry.toLowerCase().contains(value)) {
								subentries.add(clientsTable.getItems().get(i));
								break;
							}
						}
					}
					clientsTable.setItems(subentries);
					filtered.setText(subentries.size() + " Filtrados");
				});
		total.setText(count + " Clientes");
	}

	@FXML
	private void file() {
		ObservableList<Client> clientsSelected;
		clientsSelected = clientsTable.getSelectionModel().getSelectedItems();

		screenRequired(clientsSelected.get(0).getName());
	}

	@FXML
	private void create() {
		screenRequired("NewClient");
	}

	private void delete(int ID, int index) {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("DELETE from clients WHERE ID=?");
			stmt.setInt(1, ID);
			stmt.execute();

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		ObservableList<Client> allClients = clientsTable.getItems();
		allClients.remove(index);

		count--;
		total.setText(count + " Clientes");
	}

	@FXML
	private void delete() {
		ObservableList<Client> clientsSelected, allClients;
		allClients = clientsTable.getItems();
		clientsSelected = clientsTable.getSelectionModel().getSelectedItems();
		int clientsDeleted = clientsSelected.size();

		try {
			String sql = "DELETE from clients WHERE ID in(";

			for (int i = 0; i < clientsDeleted; i++) {
				sql += "?";
				if (clientsDeleted != i + 1)
					sql += ",";
			}
			sql += ");";
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql);

			for (int i = 1; i <= clientsDeleted; i++) {
				stmt.setInt(i, clientsSelected.get(i - 1).getID());
			}
			stmt.execute();

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		allClients.removeAll(clientsSelected);
		clientsTable.getSelectionModel().clearSelection();

		count -= clientsDeleted;
		total.setText(count + " Clientes");
	}

	public ObservableList<Client> getClients() {
		try {
			Connection conn = dbConnection.getConnection();
			ResultSet rs = conn.createStatement().executeQuery("SELECT * FROM clients");

			ObservableList<Client> clients = FXCollections.observableArrayList();
			count = 0;
			while (rs.next()) {
				count++;
				clients.add(new Client(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9)));
			}
			rs.close();
			conn.close();
			
			return clients;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}