package tables;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import entities.Supplier;
import files.SupplierFileController;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import utils.dbConnection;

public class SuppliersController implements Initializable {
	
	@FXML
	VBox tableContainer;
	@FXML
	TextField filter;
	@FXML
	Label total, filtered, selected;
	@FXML
	ContextMenu contextMenu;

	TableView<Supplier> suppliersTable = new TableView<Supplier>();
	
	private int count;
	
	private static StringProperty screenRequired = new SimpleStringProperty();
	public static StringProperty screenRequiredProperty() {
        return screenRequired;
    }
    private void screenRequired(String screen) {
    	screenRequiredProperty().set(screen);
    }
    
	@SuppressWarnings("unchecked")
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		count = 0;

		TableColumn<Supplier, String> IDCol = new TableColumn<>("ID");
		IDCol.setCellValueFactory(new PropertyValueFactory<>("ID"));
		TableColumn<Supplier, String> nameCol = new TableColumn<>("Nombre");
		nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
		TableColumn<Supplier, String> townCol = new TableColumn<>("Poblaci�n");
		townCol.setCellValueFactory(new PropertyValueFactory<>("town"));
		TableColumn<Supplier, String> provinceCol = new TableColumn<>("Provincia");
		provinceCol.setCellValueFactory(new PropertyValueFactory<>("province"));
		TableColumn<Supplier, String> contactCol = new TableColumn<>("Contacto");
		contactCol.setCellValueFactory(new PropertyValueFactory<>("contact"));
		TableColumn<Supplier, String> phoneNumberCol = new TableColumn<>("Tel�fono");
		phoneNumberCol.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
		TableColumn<Supplier, String> registrationCol = new TableColumn<>("Alta");
		registrationCol.setCellValueFactory(new PropertyValueFactory<>("registration"));
		TableColumn<Supplier, String> categoryCol = new TableColumn<>("Categor�a");
		categoryCol.setCellValueFactory(new PropertyValueFactory<>("category"));
		TableColumn<Supplier, String> actionCol = new TableColumn<>("Acciones");
		actionCol.setCellValueFactory(new PropertyValueFactory<>(""));

		Callback<TableColumn<Supplier, String>, TableCell<Supplier, String>> cellFactory = new Callback<TableColumn<Supplier, String>, TableCell<Supplier, String>>() {
			@Override
			public TableCell<Supplier, String> call(final TableColumn<Supplier, String> param) {
				final TableCell<Supplier, String> cell = new TableCell<Supplier, String>() {
					@Override
					public void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							JFXButton fileButton = new JFXButton();
							fileButton.getStyleClass().add("success");
							fileButton.setOnAction(event -> screenRequired(getTableView().getItems().get(getIndex()).getName()));					
							FontAwesomeIconView fileIcon = new FontAwesomeIconView();
							fileIcon.setIcon(FontAwesomeIcon.USER);
							fileIcon.setStyle("-fx-fill: #4AA84E;");
							fileIcon.setSize("15");
							fileButton.setGraphic(fileIcon);

							JFXButton deleteButton = new JFXButton();
							deleteButton.getStyleClass().add("error");
							deleteButton.setOnAction(event -> delete(getTableView().getItems().get(getIndex()).getID(), getIndex()));
							FontAwesomeIconView deleteIcon = new FontAwesomeIconView();
							deleteIcon.setIcon(FontAwesomeIcon.CLOSE);
							deleteIcon.setStyle("-fx-fill: #F44336;");
							deleteIcon.setSize("15");
							deleteButton.setGraphic(deleteIcon);

							HBox hb = new HBox();
							hb.getChildren().addAll(fileButton, deleteButton);
							hb.setAlignment(Pos.CENTER);

							setGraphic(hb);
						}
						setText(null);
					}
				};
				return cell;
			}
		};
		actionCol.setCellFactory(cellFactory);

		suppliersTable = new TableView<>();
		suppliersTable.getColumns().addAll(IDCol, nameCol, townCol, contactCol, phoneNumberCol, 
											registrationCol, categoryCol, actionCol);

		tableContainer.getChildren().addAll(suppliersTable);
		VBox.setVgrow(suppliersTable, Priority.ALWAYS);
		
		suppliersTable.setRowFactory(tv -> {
			TableRow<Supplier> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				ObservableList<Supplier> selected;
				selected = suppliersTable.getSelectionModel().getSelectedItems();
				this.selected.setText(selected.size() + " Seleccionados");

				if (event.getClickCount() == 2 && (!row.isEmpty()))
					file();
			});
			return row;
		});

		suppliersTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		suppliersTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		suppliersTable.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.isSecondaryButtonDown())
					contextMenu.show(suppliersTable, event.getScreenX(), event.getScreenY());
			}
		});

		refreshTable();
		
		SupplierFileController.screenRequiredProperty().addListener((obs, oldScreenRequired, newScreenRequired) -> {
			if (newScreenRequired != "") {
				SupplierFileController.screenRequiredProperty().set("");
				screenRequired("Suppliers");
				refreshTable();
			}
		});
	}

	public void refreshTable() {
		suppliersTable.setItems(getSuppliers());

		filter.setText("");

		ObservableList<Supplier> data = suppliersTable.getItems();
		filter.textProperty()
				.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
					if (oldValue != null && (newValue.length() < oldValue.length()))
						suppliersTable.setItems(data);

					String value = newValue.toLowerCase();
					ObservableList<Supplier> subentries = FXCollections.observableArrayList();

					long count = suppliersTable.getColumns().stream().count();
					for (int i = 0; i < suppliersTable.getItems().size(); i++) {
						for (int j = 0; j < count; j++) {
							String entry = "" + suppliersTable.getColumns().get(j).getCellData(i);
							if (entry.toLowerCase().contains(value)) {
								subentries.add(suppliersTable.getItems().get(i));
								break;
							}
						}
					}
					suppliersTable.setItems(subentries);
					filtered.setText(subentries.size() + " Filtrados");
				});
		total.setText(count + " Proveedores");
	}

	@FXML
	private void file() {
		ObservableList<Supplier> suppliersSelected;
		suppliersSelected = suppliersTable.getSelectionModel().getSelectedItems();

		screenRequired(suppliersSelected.get(0).getName());
	}

	@FXML
	private void create() {
		screenRequired("NewSupplier");
	}

	private void delete(int ID, int index) {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("DELETE from suppliers WHERE ID=?");
			stmt.setInt(1, ID);
			stmt.execute();

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		ObservableList<Supplier> allSuppliers = suppliersTable.getItems();
		allSuppliers.remove(index);

		count--;
		total.setText(count + " Proveedores");
	}

	@FXML
	private void delete() {
		ObservableList<Supplier> suppliersSelected, allSuppliers;
		allSuppliers = suppliersTable.getItems();
		suppliersSelected = suppliersTable.getSelectionModel().getSelectedItems();
		int suppliersDeleted = suppliersSelected.size();

		try {
			String sql = "DELETE from suppliers WHERE ID in(";

			for (int i = 0; i < suppliersDeleted; i++) {
				sql += "?";
				if (suppliersDeleted != i + 1)
					sql += ",";
			}
			sql += ");";
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql);

			for (int i = 1; i <= suppliersDeleted; i++) {
				stmt.setInt(i, suppliersSelected.get(i - 1).getID());
			}
			stmt.execute();

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		allSuppliers.removeAll(suppliersSelected);
		suppliersTable.getSelectionModel().clearSelection();

		count -= suppliersDeleted;
		total.setText(count + " Proveedores");
	}

	public ObservableList<Supplier> getSuppliers() {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			
			Connection conn = dbConnection.getConnection();
			ResultSet rs = conn.createStatement().executeQuery("SELECT ID, name, town, "
				+ "province, contact, phoneNumber, registration, category FROM suppliers");

			ObservableList<Supplier> suppliers = FXCollections.observableArrayList();
			count = 0;
			while (rs.next()) {
				count++;
				suppliers.add(new Supplier(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), LocalDate.parse(rs.getString(7), formatter), rs.getString(8)));
			}
			rs.close();
			conn.close();
			
			return suppliers;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}