package tables;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import entities.Paper;
import files.PaperFileController;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import utils.dbConnection;

public class PapersController implements Initializable {
	
	@FXML
	VBox tableContainer;
	@FXML
	TextField filter;
	@FXML
	Label total, filtered, selected;
	@FXML
	ContextMenu contextMenu;

	TableView<Paper> papersTable = new TableView<Paper>();
	
	private int count;
	
	private static StringProperty screenRequired = new SimpleStringProperty();
	public static StringProperty screenRequiredProperty() {
        return screenRequired;
    }
    private void screenRequired(String screen) {
    	screenRequiredProperty().set(screen);
    }
    
	@SuppressWarnings("unchecked")
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		count = 0;

		TableColumn<Paper, String> nameCol = new TableColumn<>("Nombre");
		nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
		TableColumn<Paper, String> supplierCol = new TableColumn<>("Proveedor");
		supplierCol.setCellValueFactory(new PropertyValueFactory<>("supplier"));
		TableColumn<Paper, String> paperTypeCol = new TableColumn<>("Tipo de Papel");
		paperTypeCol.setCellValueFactory(new PropertyValueFactory<>("papelType"));
		TableColumn<Paper, String> grammageCol = new TableColumn<>("Gramaje");
		grammageCol.setCellValueFactory(new PropertyValueFactory<>("grammage"));
		TableColumn<Paper, String> dimensionsCol = new TableColumn<>("Dimensiones");
		dimensionsCol.setCellValueFactory(new PropertyValueFactory<>("dimensions"));
		TableColumn<Paper, String> unitCol = new TableColumn<>("Unidad");
		unitCol.setCellValueFactory(new PropertyValueFactory<>("unit"));
		TableColumn<Paper, String> unitaryCostCol = new TableColumn<>("Coste Unitario");
		unitaryCostCol.setCellValueFactory(new PropertyValueFactory<>("unitaryCost"));
		TableColumn<Paper, String> actionCol = new TableColumn<>("Acciones");
		actionCol.setCellValueFactory(new PropertyValueFactory<>(""));

		Callback<TableColumn<Paper, String>, TableCell<Paper, String>> cellFactory = new Callback<TableColumn<Paper, String>, TableCell<Paper, String>>() {
			@Override
			public TableCell<Paper, String> call(final TableColumn<Paper, String> param) {
				final TableCell<Paper, String> cell = new TableCell<Paper, String>() {
					@Override
					public void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							JFXButton fileButton = new JFXButton();
							fileButton.getStyleClass().add("success");
							fileButton.setOnAction(event -> screenRequired(getTableView().getItems().get(getIndex()).getName()));						
							FontAwesomeIconView fileIcon = new FontAwesomeIconView();
							fileIcon.setIcon(FontAwesomeIcon.USER);
							fileIcon.setStyle("-fx-fill: #4AA84E;");
							fileIcon.setSize("15");
							fileButton.setGraphic(fileIcon);

							JFXButton deleteButton = new JFXButton();
							deleteButton.getStyleClass().add("error");
							deleteButton.setOnAction(event -> delete(getTableView().getItems().get(getIndex()).getID(), getIndex()));
							FontAwesomeIconView deleteIcon = new FontAwesomeIconView();
							deleteIcon.setIcon(FontAwesomeIcon.CLOSE);
							deleteIcon.setStyle("-fx-fill: #F44336;");
							deleteIcon.setSize("15");
							deleteButton.setGraphic(deleteIcon);

							HBox hb = new HBox();
							hb.getChildren().addAll(fileButton, deleteButton);
							hb.setAlignment(Pos.CENTER);

							setGraphic(hb);
						}
						setText(null);
					}
				};
				return cell;
			}
		};
		actionCol.setCellFactory(cellFactory);

		papersTable = new TableView<>();
		papersTable.getColumns().addAll(nameCol, supplierCol, paperTypeCol,
				grammageCol, dimensionsCol, unitCol, unitaryCostCol, actionCol);

		tableContainer.getChildren().addAll(papersTable);
		VBox.setVgrow(papersTable, Priority.ALWAYS);
		
		papersTable.setRowFactory(tv -> {
			TableRow<Paper> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				ObservableList<Paper> selected;
				selected = papersTable.getSelectionModel().getSelectedItems();
				this.selected.setText(selected.size() + " Seleccionados");

				if (event.getClickCount() == 2 && (!row.isEmpty()))
					file();
			});
			return row;
		});

		papersTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		papersTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		papersTable.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.isSecondaryButtonDown())
					contextMenu.show(papersTable, event.getScreenX(), event.getScreenY());
			}
		});

		refreshTable();
		
		PaperFileController.screenRequiredProperty().addListener((obs, oldScreenRequired, newScreenRequired) -> {
			if (newScreenRequired != "") {
				PaperFileController.screenRequiredProperty().set("");
				screenRequired("Papers");
				refreshTable();
			}
		});
	}

	public void refreshTable() {
		papersTable.setItems(getPapers());

		filter.setText("");

		ObservableList<Paper> data = papersTable.getItems();
		filter.textProperty()
				.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
					if (oldValue != null && (newValue.length() < oldValue.length()))
						papersTable.setItems(data);

					String value = newValue.toLowerCase();
					ObservableList<Paper> subentries = FXCollections.observableArrayList();

					long count = papersTable.getColumns().stream().count();
					for (int i = 0; i < papersTable.getItems().size(); i++) {
						for (int j = 0; j < count; j++) {
							String entry = "" + papersTable.getColumns().get(j).getCellData(i);
							if (entry.toLowerCase().contains(value)) {
								subentries.add(papersTable.getItems().get(i));
								break;
							}
						}
					}
					papersTable.setItems(subentries);
					filtered.setText(subentries.size() + " Filtrados");
				});
		total.setText(count + " Papeles");
	}

	@FXML
	private void file() {
		ObservableList<Paper> papersSelected;
		papersSelected = papersTable.getSelectionModel().getSelectedItems();

		screenRequired(papersSelected.get(0).getName());
	}

	@FXML
	private void create() {
		screenRequired("NewPaper");
	}

	private void delete(int ID, int index) {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("DELETE from papers WHERE ID=?");
			stmt.setInt(1, ID);
			stmt.execute();

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		ObservableList<Paper> allPapers = papersTable.getItems();
		allPapers.remove(index);

		count--;
		total.setText(count + " Papeles");
	}

	@FXML
	private void delete() {
		ObservableList<Paper> papersSelected, allPapers;
		allPapers = papersTable.getItems();
		papersSelected = papersTable.getSelectionModel().getSelectedItems();
		int papersDeleted = papersSelected.size();

		try {
			String sql = "DELETE from papers WHERE ID in(";

			for (int i = 0; i < papersDeleted; i++) {
				sql += "?";
				if (papersDeleted != i + 1)
					sql += ",";
			}
			sql += ");";
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql);

			for (int i = 1; i <= papersDeleted; i++) {
				stmt.setInt(i, papersSelected.get(i - 1).getID());
			}
			stmt.execute();

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		allPapers.removeAll(papersSelected);
		papersTable.getSelectionModel().clearSelection();

		count -= papersDeleted;
		total.setText(count + " Papeles");
	}

	public ObservableList<Paper> getPapers() {
		try {
			Connection conn = dbConnection.getConnection();
			ResultSet rs = conn.createStatement().executeQuery("SELECT name, supplier, "
				+ "paperType, grammage, dimensions, unit, unitaryCost FROM papers");

			ObservableList<Paper> papers = FXCollections.observableArrayList();
			count = 0;
			while (rs.next()) {
				count++;
				papers.add(new Paper(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getString(7)));
			}
			rs.close();
			conn.close();
			
			return papers;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}