package tables;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import entities.Machine;
import files.MachineFileController;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import utils.dbConnection;

public class MachinesController implements Initializable {
	
	@FXML
	VBox tableContainer;
	@FXML
	TextField filter;
	@FXML
	Label total, filtered, selected;
	@FXML
	ContextMenu contextMenu;

	TableView<Machine> machinesTable = new TableView<Machine>();
	
	private int count;
	
	private static StringProperty screenRequired = new SimpleStringProperty();
	public static StringProperty screenRequiredProperty() {
        return screenRequired;
    }
    private void screenRequired(String screen) {
    	screenRequiredProperty().set(screen);
    }
    
	@SuppressWarnings("unchecked")
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		count = 0;

		TableColumn<Machine, String> col1 = new TableColumn<>("Nombre");
		col1.setCellValueFactory(new PropertyValueFactory<>("name"));
		TableColumn<Machine, String> col2 = new TableColumn<>("Fabricante y Modelo");
		col2.setCellValueFactory(new PropertyValueFactory<>("manufacturerAndModel"));
		TableColumn<Machine, String> col3 = new TableColumn<>("Formato m�ximo");
		col3.setCellValueFactory(new PropertyValueFactory<>("maxDimensions"));
		TableColumn<Machine, String> col4 = new TableColumn<>("Colores");
		col4.setCellValueFactory(new PropertyValueFactory<>("colors"));
		TableColumn<Machine, String> col5 = new TableColumn<>("Fecha de costes");
		col5.setCellValueFactory(new PropertyValueFactory<>("costsDate"));
		TableColumn<Machine, String> actionCol = new TableColumn<>("Acciones");
		actionCol.setCellValueFactory(new PropertyValueFactory<>(""));
		
		Callback<TableColumn<Machine, String>, TableCell<Machine, String>> cellFactory = new Callback<TableColumn<Machine, String>, TableCell<Machine, String>>() {
			@Override
			public TableCell<Machine, String> call(final TableColumn<Machine, String> param) {
				final TableCell<Machine, String> cell = new TableCell<Machine, String>() {
					@Override
					public void updateItem(String item, boolean empty) {
						super.updateItem(item, empty);
						if (empty) {
							setGraphic(null);
						} else {
							JFXButton fileButton = new JFXButton();
							fileButton.getStyleClass().add("success");
							fileButton.setOnAction(event -> screenRequired(getTableView().getItems().get(getIndex()).getName()));						
							FontAwesomeIconView fileIcon = new FontAwesomeIconView();
							fileIcon.setIcon(FontAwesomeIcon.USER);
							fileIcon.setStyle("-fx-fill: #4AA84E;");
							fileIcon.setSize("15");
							fileButton.setGraphic(fileIcon);

							JFXButton deleteButton = new JFXButton();
							deleteButton.getStyleClass().add("error");
							deleteButton.setOnAction(event -> delete(getTableView().getItems().get(getIndex()).getID(), getIndex()));
							FontAwesomeIconView deleteIcon = new FontAwesomeIconView();
							deleteIcon.setIcon(FontAwesomeIcon.CLOSE);
							deleteIcon.setStyle("-fx-fill: #F44336;");
							deleteIcon.setSize("15");
							deleteButton.setGraphic(deleteIcon);

							HBox hb = new HBox();
							hb.getChildren().addAll(fileButton, deleteButton);
							hb.setAlignment(Pos.CENTER);

							setGraphic(hb);
						}
						setText(null);
					}
				};
				return cell;
			}
		};
		actionCol.setCellFactory(cellFactory);

		machinesTable = new TableView<>();
		machinesTable.getColumns().addAll(col1, col2, col3, col4, col5, actionCol);

		tableContainer.getChildren().addAll(machinesTable);
		VBox.setVgrow(machinesTable, Priority.ALWAYS);
		
		machinesTable.setRowFactory(tv -> {
			TableRow<Machine> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				ObservableList<Machine> selected;
				selected = machinesTable.getSelectionModel().getSelectedItems();
				this.selected.setText(selected.size() + " Seleccionados");

				if (event.getClickCount() == 2 && (!row.isEmpty()))
					file();
			});
			return row;
		});

		machinesTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		machinesTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		machinesTable.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.isSecondaryButtonDown())
					contextMenu.show(machinesTable, event.getScreenX(), event.getScreenY());
			}
		});

		refreshTable();
		
		MachineFileController.screenRequiredProperty().addListener((obs, oldScreenRequired, newScreenRequired) -> {
			if (newScreenRequired != "") {
				MachineFileController.screenRequiredProperty().set("");
				screenRequired("Machines");
				refreshTable();
			}
		});
	}

	public void refreshTable() {
		machinesTable.setItems(getMachines());

		filter.setText("");

		ObservableList<Machine> data = machinesTable.getItems();
		filter.textProperty()
				.addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
					if (oldValue != null && (newValue.length() < oldValue.length()))
						machinesTable.setItems(data);

					String value = newValue.toLowerCase();
					ObservableList<Machine> subentries = FXCollections.observableArrayList();

					long count = machinesTable.getColumns().stream().count();
					for (int i = 0; i < machinesTable.getItems().size(); i++) {
						for (int j = 0; j < count; j++) {
							String entry = "" + machinesTable.getColumns().get(j).getCellData(i);
							if (entry.toLowerCase().contains(value)) {
								subentries.add(machinesTable.getItems().get(i));
								break;
							}
						}
					}
					machinesTable.setItems(subentries);
					filtered.setText(subentries.size() + " Filtradas");
				});
		total.setText(count + " M�quinas");
	}

	@FXML
	private void file() {
		ObservableList<Machine> machinesSelected;
		machinesSelected = machinesTable.getSelectionModel().getSelectedItems();

		screenRequired(machinesSelected.get(0).getName());
	}

	@FXML
	private void create() {
		screenRequired("NewMachine");
	}

	private void delete(int ID, int index) {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("DELETE from machines WHERE ID=?");
			stmt.setInt(1, ID);
			stmt.execute();

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		ObservableList<Machine> allMachines = machinesTable.getItems();
		allMachines.remove(index);

		count--;
		total.setText(count + " M�quinas");
	}

	@FXML
	private void delete() {
		ObservableList<Machine> machinesSelected, allMachines;
		allMachines = machinesTable.getItems();
		machinesSelected = machinesTable.getSelectionModel().getSelectedItems();
		int machinesDeleted = machinesSelected.size();

		try {
			String sql = "DELETE from machines WHERE ID in(";

			for (int i = 0; i < machinesDeleted; i++) {
				sql += "?";
				if (machinesDeleted != i + 1)
					sql += ",";
			}
			sql += ");";
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement(sql);

			for (int i = 1; i <= machinesDeleted; i++) {
				stmt.setInt(i, machinesSelected.get(i - 1).getID());
			}
			stmt.execute();

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		allMachines.removeAll(machinesSelected);
		machinesTable.getSelectionModel().clearSelection();

		count -= machinesDeleted;
		total.setText(count + " M�quinas");
	}

	public ObservableList<Machine> getMachines() {
		try {
			Connection conn = dbConnection.getConnection();
			ResultSet rs = conn.createStatement().executeQuery("SELECT name, manufacturerAndModel, "
				+ "maxDimensions, colors, costsDate FROM machines");
			
			ObservableList<Machine> machines = FXCollections.observableArrayList();
			count = 0;
			while (rs.next()) {
				count++;
				machines.add(new Machine(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4),
						LocalDate.parse(rs.getString(5), DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
			}
			rs.close();
			conn.close();
			
			return machines;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}