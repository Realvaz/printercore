package utils;

import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/** demonstrates various ways to fill a constrained region with an image */
public class ImageFill extends Application {
	
  private static final String FX_EXPERIENCE_LOGO_URL = "http://fxexperience.com/wp-content/uploads/2010/06/logo.png";
  
  final ObjectProperty<Image> poster = new SimpleObjectProperty<Image>(new Image(FX_EXPERIENCE_LOGO_URL));  
  
  public static void main(String[] args) { launch(args); }
  
  @Override public void start(Stage primaryStage) {
    StackPane constrainingPane = new StackPane();

    constrainingPane.getChildren().add(
      new ImagePane(FX_EXPERIENCE_LOGO_URL, "-fx-background-size: contain; -fx-background-repeat: no-repeat;")
    );
    
    // layout the scene.
    StackPane layout = new StackPane();
    layout.getChildren().add(constrainingPane);
    layout.setStyle("-fx-background-color: whitesmoke;");
    Scene scene = new Scene(layout, 800, 600);

    // clamp the pane to the scene size.
    constrainingPane.maxWidthProperty().bind(scene.widthProperty());
    constrainingPane.minWidthProperty().bind(scene.widthProperty());
    constrainingPane.maxHeightProperty().bind(scene.heightProperty());
    constrainingPane.minHeightProperty().bind(scene.heightProperty());

    // show the scene.
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}

class ImagePane extends Pane {
  ImagePane(String imageLoc, String style) {
    this(new SimpleStringProperty(imageLoc), new SimpleStringProperty(style));
  }

  ImagePane(StringProperty imageLocProperty, StringProperty styleProperty) {
    styleProperty().bind(
    new SimpleStringProperty("-fx-background-image: url(\"")
        .concat(imageLocProperty)
        .concat(new SimpleStringProperty("\");"))
        .concat(styleProperty)    
    );
  }
}