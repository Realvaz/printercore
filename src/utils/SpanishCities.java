package utils;

public enum SpanishCities {
	A_CORUNA("A Coru�a"), ALAVA("�lava"), ALBACETE("Albacete"), ALICANTE("Alicante"), ALMERIA("Almer�a"), ASTURIAS("Asturias"),
	AVILA("�vila"), BADAJOZ("Badajoz"), ISLAS_BALEARES("Islas Baleares"), BARCELONA("Barcelona"), BURGOS("Burgos"), CACERES("C�ceres"),
	CADIZ("C�diz"), CANTABRIA("Cantabria"), CASTELLON("Castell�n"), CIUDAD_REAL("Ciudad Real"), CORDOBA("C�rdoba"), CUENCA("Cuenca"),
	Girona("Girona"), GRANADA("Granada"), GUADALAJARA("Guadalajara"), GUIPUZCOA("Guip�zcoa"), HUELVA("Huelva"), HUESCA("Huesca"),
	JAEN("Ja�n"), LA_RIOJA("La Rioja"), LAS_PALMAS("Las Palmas"), LEON("Le�n"), LLEIDA("Lleida"), LUGO("Lugo"), MADRID("Madrid"),
	MALAGA("M�laga"), MURCIA("Murcia"), NAVARRA("Navarra"), ORENSE("Orense"), PALENCIA("Palencia"), PONTEVEDRA("Pontevedra"),
	SALAMANCA("Salamanca"), SEGOVIA("Segovia"), SEVILLA("Sevilla"), SORIA("Soria"), TARRAGONA("Tarragona"),
	SANTA_CRUZ_DE_TENERIFE("Santa Cruz de Tenerife"), TERUEL("Teruel"), TOLEDO("Toledo"), VALENCIA("Valencia"), VALLADOLID("Valladolid"),
	VIZCAYA("Vizcaya"), ZAMORA("Zamora"), ZARAGOZA("Zaragoza");

	private final String name;
	
	SpanishCities(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}