//package login;
//
//import java.net.URL;
//import java.util.ResourceBundle;
//
//import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
//import javafx.event.ActionEvent;
//import javafx.fxml.FXML;
//import javafx.fxml.FXMLLoader;
//import javafx.fxml.Initializable;
//import javafx.scene.Parent;
//import javafx.scene.Scene;
//import javafx.scene.control.Button;
//import javafx.scene.control.Label;
//import javafx.scene.control.PasswordField;
//import javafx.scene.control.TextField;
//import javafx.scene.input.MouseEvent;
//import javafx.stage.Screen;
//import javafx.stage.Stage;
//
//public class LoginController extends LoginMain implements Initializable{
//	
//	LoginModel loginModel = new LoginModel();
//	
//	@FXML
//	private Label incorrect_text;
//	@FXML
//	private Label connectionStatus;
//	@FXML
//	private FontAwesomeIconView incorrect_icon;
//	@FXML
//	private Button loginButton;
//	@FXML
//	private TextField username;
//	@FXML
//	private PasswordField password;
//	  	  
//	double xMouse, yMouse;
//	
//	@FXML
//	public void Login(ActionEvent event) {
//		try {
//			if (this.loginModel.isLogin(this.username.getText(), this.password.getText())) {
//
//				Stage stage = (Stage) this.loginButton.getScene().getWindow();
//				stage.close();
//				
//				Stage home = new Stage();
//				Parent root = FXMLLoader.load(getClass().getResource("../home/home.fxml"));
//				Scene scene = new Scene(root);
//				home.setScene(scene);	
//				home.setTitle("SmartPrint 2018 - Home");
//				home.show();
//				home.setX((Screen.getPrimary().getVisualBounds().getWidth()-home.getWidth())/2);
//				home.setY((Screen.getPrimary().getVisualBounds().getHeight()-home.getHeight())/2);
//			      
//			} else {
//				this.username.setText("");
//				this.password.setText("");
//				this.incorrect_text.setVisible(true);
//				this.incorrect_icon.setVisible(true);
//			}
//		} catch (Exception e) {
//			System.out.println(e);
//		}
//	}
//	  
//	@FXML
//	private void handleClose(MouseEvent event) {
//		System.exit(0);
//	}
//	
//	@FXML
//	private void handleMotionDrag(MouseEvent event) {
//
//		double x = event.getScreenX();
//		double y = event.getScreenY();
//
//		window.setX(x-xMouse);
//		window.setY(y-yMouse);
//	}
//	
//	@FXML
//	private void handleMotionPress(MouseEvent event) {
//
//		xMouse = event.getSceneX();
//		yMouse = event.getSceneY();
//	}
//	
//	@Override
//	public void initialize(URL arg0, ResourceBundle arg1) {
//	}
//}