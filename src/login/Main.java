package login;
	
import java.util.Locale;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) {	    
	    
		try {  
			Locale.setDefault(new Locale("ES"));
			
			Stage homeStage = new Stage();
			homeStage.setTitle("PrinterCore");
			Parent root = FXMLLoader.load(getClass().getResource("/home/Home.fxml"));
			Scene scene = new Scene(root);
			homeStage.setScene(scene);
			homeStage.show();
			homeStage.setMinHeight(450);
			homeStage.setMinWidth(400);
			// We have to set X and Y after .show to center correctly
			homeStage.setX((Screen.getPrimary().getVisualBounds().getWidth()-homeStage.getWidth())/2);
			homeStage.setY((Screen.getPrimary().getVisualBounds().getHeight()-homeStage.getHeight())/2);

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		launch(args);
	}
}