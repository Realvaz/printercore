package files;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXRadioButton;

import entities.Paper;
import entities.Supplier;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import utils.dbConnection;

public class MachineFileController implements Initializable {

	@FXML
	TextField ID, name, paperType, grammage, dimensions, unit,
		unitaryCost, description, storeLocation, lot, reference;
	@FXML
	TextArea extraInfo;
	@FXML
	JFXDatePicker costsDate;
	@FXML
	ComboBox<String> supplier;
	@FXML
	JFXRadioButton sheetPrint, unitPrint, reelPrint;
	@FXML
	Label nameLabel, descriptionLabel, ordersLabel, investedLabel, suppliesLabel, alreadyExists;
	@FXML
	VBox imageContainer;
	@FXML
	ImageView background, profile;
	
	private String oldName;
	
	private static StringProperty screenRequired = new SimpleStringProperty();
	public static StringProperty screenRequiredProperty() {
        return screenRequired;
    }
    private void screenRequired(String screen) {
    	screenRequiredProperty().set(screen);
    }
    
	public void initData(String name) {
		oldName = name;

		this.name.getStyleClass().removeAll("error");
		alreadyExists.setManaged(false);
		alreadyExists.setVisible(false);
		
		if(!name.isEmpty()) {
			Paper paper = new Paper(name);
			
			ID.setText(Integer.toString(paper.getID()));
			this.name.setText(paper.getName());
			supplier.setValue(paper.getSupplier());
			paperType.setText(paper.getPaperType());
			grammage.setText(paper.getGrammage());
			dimensions.setText(paper.getDimensions());
			unit.setText(paper.getUnit());
			unitaryCost.setText(paper.getUnitaryCost());
			sheetPrint.setSelected(paper.getSheetPrint());
			unitPrint.setSelected(paper.getUnitPrint());
			reelPrint.setSelected(paper.getReelPrint());
			description.setText(paper.getDescription());
			costsDate.setValue(paper.getCostsDate());
			storeLocation.setText(paper.getStoreLocation());
			lot.setText(paper.getLot());
			reference.setText(paper.getReference());
			extraInfo.setText(paper.getExtraInfo());
			
		} else {
			try {
				Connection conn = dbConnection.getConnection();
				PreparedStatement stmt = conn.prepareStatement("SELECT seq FROM sqlite_sequence WHERE name=?");
				stmt.setString(1, "papers");
				ResultSet rs = stmt.executeQuery();
				
				ID.setText(Integer.toString(Integer.parseInt(rs.getString("seq"))+1));

				rs.close();
				stmt.close();
				conn.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			this.name.clear();
			paperType.clear();
			grammage.clear();
			unit.clear();
			unitaryCost.clear();
			sheetPrint.setSelected(true);
			unitPrint.setSelected(false);
			reelPrint.setSelected(false);
			description.clear();
			costsDate.setValue(LocalDate.now());
			storeLocation.clear();
			lot.clear();
			reference.clear();
			extraInfo.clear();
		}
	}
	  
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Circle clipProfile = new Circle(55, 55, 55);
		profile.setClip(clipProfile);
        
		extraInfo.textProperty().addListener((obs2, oldName, newName) -> 
			descriptionLabel.setText(newName));
		
		alreadyExists.setManaged(false);
		alreadyExists.setVisible(false);
		
		for(String item: Supplier.getAll())
			supplier.getItems().add(item);
	}
	
	@FXML
	private void save() {
		Paper paper = new Paper(
				Integer.parseInt(ID.getText()), name.getText(),
				(String)supplier.getValue(), paperType.getText(),
				grammage.getText(), dimensions.getText(), unit.getText(),
				unitaryCost.getText(), sheetPrint.isSelected(),
				unitPrint.isSelected(), reelPrint.isSelected(),
				description.getText(), costsDate.getValue(),
				storeLocation.getText(), lot.getText(),
				reference.getText(), extraInfo.getText());

		if(paper.alreadyInDB()) {
			name.getStyleClass().add("error");
			alreadyExists.setManaged(true);
			alreadyExists.setVisible(true);
		} else {
			if(oldName.isEmpty())
				paper.insert();
			else
				paper.update();
			
			screenRequired("Papers");
		}
	}
}