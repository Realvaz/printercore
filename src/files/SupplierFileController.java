package files;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

import org.controlsfx.control.textfield.TextFields;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXToggleButton;

import entities.Supplier;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import utils.SpanishCities;
import utils.dbConnection;

public class SupplierFileController implements Initializable {

	@FXML
	TextField ID, name, town, province, contact, phoneNumber, category,
		businessName, fax, email, webPage, extraName1, extraEmail1, extraNumber1,
		extraName2, extraEmail2, extraNumber2, address;
	@FXML
	TextArea description;
	@FXML
	JFXDatePicker registration;
	@FXML
	JFXToggleButton toggleName;
	@FXML
	JFXCheckBox checkboxExtraContact;
	@FXML
	Label nameLabel, descriptionLabel, ordersLabel, investedLabel, suppliesLabel, alreadyExists;
	@FXML
	VBox imageContainer, addressContainer;
	@FXML
	HBox extraContactContainer;
	@FXML
	ImageView profile;
	
	private String oldName;
	
	private static StringProperty screenRequired = new SimpleStringProperty();
	public static StringProperty screenRequiredProperty() {
        return screenRequired;
    }
    private void screenRequired(String screen) {
    	screenRequiredProperty().set(screen);
    }
    
	public void initData(String name) {
		oldName = name;

		this.name.getStyleClass().removeAll("error");
		alreadyExists.setManaged(false);
		alreadyExists.setVisible(false);
		
		if(!name.isEmpty()) {
			Supplier supplier = new Supplier(name);
			
			ID.setText(Integer.toString(supplier.getID()));
			this.name.setText(supplier.getName());
			town.setText(supplier.getTown());
			province.setText(supplier.getProvince());
			contact.setText(supplier.getContact());
			phoneNumber.setText(supplier.getPhoneNumber());
			category.setText(supplier.getCategory());
			businessName.setText(supplier.getBusinessName());
			businessName.setDisable(true);
			fax.setText(supplier.getFax());
			email.setText(supplier.getEmail());
			webPage.setText(supplier.getWebPage());
			extraName1.setText(supplier.getExtraName1());
			extraEmail1.setText(supplier.getExtraEmail1());
			extraNumber1.setText(supplier.getExtraNumber1());
			extraName2.setText(supplier.getExtraName2());
			extraEmail2.setText(supplier.getExtraEmail2());
			extraNumber2.setText(supplier.getExtraNumber2());
			address.setText(supplier.getAddress());
			description.setText(supplier.getDescription());
			registration.setValue(supplier.getRegistration());
			
		} else {
			try {
				Connection conn = dbConnection.getConnection();
				PreparedStatement stmt = conn.prepareStatement("SELECT seq FROM sqlite_sequence WHERE name=?");
				stmt.setString(1, "suppliers");
				ResultSet rs = stmt.executeQuery();
				
				ID.setText(Integer.toString(Integer.parseInt(rs.getString("seq"))+1));
				
				rs.close();
				stmt.close();
				conn.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			this.name.clear();
			town.clear();
			province.clear();
			contact.clear();
			phoneNumber.clear();
			category.clear();
			businessName.clear();
			businessName.setDisable(true);
			fax.clear();
			email.clear();
			webPage.clear();
			extraName1.clear();
			extraEmail1.clear();
			extraNumber1.clear();
			extraName2.clear();
			extraEmail2.clear();
			extraNumber2.clear();
			address.clear();
			description.clear();
			registration.setValue(LocalDate.now());
		}
	}
	  
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Circle clipPicture = new Circle(55, 55, 55);
		profile.setClip(clipPicture);
		
		name.textProperty().addListener((obs2, oldName, newName) -> {
			if(toggleName.isSelected()) 
				businessName.setText(newName);
			nameLabel.setText(newName);});
		
		toggleName.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
			if(isNowSelected) 
				businessName.setText(name.getText());
			businessName.setDisable(isNowSelected);});
		
		description.textProperty().addListener((obs2, oldName, newName) -> 
			descriptionLabel.setText(newName));
		
		checkboxExtraContact.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
			if(isNowSelected) {
				extraContactContainer.setManaged(true);
				extraContactContainer.setVisible(true);
				checkboxExtraContact.setPadding(new Insets(0,0,15,0));
			} else {
				extraContactContainer.setManaged(false);
				extraContactContainer.setVisible(false);
				checkboxExtraContact.setPadding(new Insets(0,0,0,0));}});
		extraContactContainer.setManaged(false);
		extraContactContainer.setVisible(false);
		
		alreadyExists.setManaged(false);
		alreadyExists.setVisible(false);

		String[] cities =  new String [SpanishCities.values().length];
		for(SpanishCities city: SpanishCities.values())
			cities[city.ordinal()] = city.getName();
		TextFields.bindAutoCompletion(province, cities);
	}
	
	@FXML
	private void saveClient() {
		Supplier supplier = new Supplier(
				Integer.parseInt(ID.getText()), name.getText(),
				town.getText(), province.getText(),
				contact.getText(), phoneNumber.getText(),
				category.getText(), businessName.getText(),
				fax.getText(), email.getText(), webPage.getText(),
				extraName1.getText(), extraNumber1.getText(),
				extraEmail1.getText(), extraName2.getText(),
				extraNumber2.getText(), extraEmail2.getText(),
				address.getText(), description.getText(),
				registration.getValue(), toggleName.isSelected());
		
		if(supplier.alreadyInDB()) {
			name.getStyleClass().add("error");
			alreadyExists.setManaged(true);
			alreadyExists.setVisible(true);
		} else {
			if(oldName.isEmpty())
				supplier.insertSupplier();
			else
				supplier.updateSupplier();

			screenRequired("Suppliers");
		}
	}
}