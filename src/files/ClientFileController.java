package files;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import org.controlsfx.control.textfield.TextFields;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXToggleButton;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import utils.SpanishCities;
import utils.dbConnection;

public class ClientFileController implements Initializable {

	@FXML
	TextField  name, ID, businessName, contact, phoneNumber, email, webPage, fax, address, town,
				province, CIF, IVA, IRPF, bankAccount, paymentMethod, paymentMethodDescription,
				paymentDeadline, paymentDays, profit, discount, budgetModel, category,
				shipmentAddress, shipmentTown, shipmentProvince, extraName1, extraName2,
				extraNumber1, extraNumber2, extraEmail1, extraEmail2;
	@FXML
	TextArea description;
	@FXML
	JFXDatePicker registration;
	@FXML
	JFXToggleButton toggleName;
	@FXML
	JFXCheckBox checkboxAddress, checkboxExtraContact, checkboxIncludeIVA;
	@FXML	
	Label descriptionLabel, nameLabel, billsLabel, earningsLabel, profitLabel, alreadyExists;
	@FXML
	VBox addressContainer, overviewContainer;
	@FXML
	HBox extraContactContainer;
	@FXML
	ImageView clientProfile;
	
	private String oldName;
	
	private static StringProperty screenRequired = new SimpleStringProperty();
	public static StringProperty screenRequiredProperty() {
        return screenRequired;
    }
    private void screenRequired(String screen) {
    	screenRequiredProperty().set(screen);
    }
    
	public void initData(String clientName) {
		oldName = clientName;
		
		if(!clientName.isEmpty()) {
			try {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				
				Connection conn = dbConnection.getConnection();
				PreparedStatement stmt = conn.prepareStatement("SELECT * FROM clients WHERE name=?");
				stmt.setString(1, oldName);
				ResultSet rs = stmt.executeQuery();
				
				ID.setText(rs.getString("ID"));
				name.setText(oldName);
				businessName.setText(rs.getString("businessName"));
				registration.setValue(LocalDate.parse(rs.getString("registration"), formatter));
				contact.setText(rs.getString("contact"));
				phoneNumber.setText(rs.getString("phoneNumber"));
				email.setText(rs.getString("email"));
				webPage.setText(rs.getString("webPage"));
				fax.setText(rs.getString("fax"));
				extraName1.setText(rs.getString("extraName1"));
				extraName2.setText(rs.getString("extraName2"));
				extraNumber1.setText(rs.getString("extraNumber1"));
				extraNumber2.setText(rs.getString("extraNumber2"));
				extraEmail1.setText(rs.getString("extraEmail1"));
				extraEmail2.setText(rs.getString("extraEmail2"));
				address.setText(rs.getString("address"));
				town.setText(rs.getString("town"));
				province.setText(rs.getString("province"));
				shipmentAddress.setText(rs.getString("shipmentAddress"));
				shipmentTown.setText(rs.getString("shipmentTown"));
				shipmentProvince.setText(rs.getString("shipmentProvince"));
				CIF.setText(rs.getString("CIF"));
				IVA.setText(rs.getString("IVA"));
				IRPF.setText(rs.getString("IRPF"));
				bankAccount.setText(rs.getString("bankAccount"));
				paymentMethod.setText(rs.getString("paymentMethod"));
				paymentMethodDescription.setText(rs.getString("paymentMethodDescription"));
				paymentDeadline.setText(rs.getString("paymentDeadline"));
				paymentDays.setText(rs.getString("paymentDays"));
				profit.setText(rs.getString("profit"));
				discount.setText(rs.getString("discount"));
				budgetModel.setText(rs.getString("budgetModel"));
				category.setText(rs.getString("category"));
				description.setText(rs.getString("description"));
				toggleName.setSelected(rs.getBoolean("toggleName"));
				businessName.setDisable(rs.getBoolean("toggleName"));
				checkboxAddress.setSelected(rs.getBoolean("checkboxAddress"));
				checkboxExtraContact.setSelected(rs.getBoolean("checkboxExtraContact"));
				checkboxIncludeIVA.setSelected(rs.getBoolean("checkboxIncludeIVA"));
	
				rs.close();
				stmt.close();
				conn.close();
					
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			try {
				Connection conn = dbConnection.getConnection();
				PreparedStatement stmt = conn.prepareStatement("SELECT seq FROM sqlite_sequence WHERE name=?");
				stmt.setString(1, "clients");
				ResultSet rs = stmt.executeQuery();
				
				ID.setText(Integer.toString(Integer.parseInt(rs.getString("seq"))+1));

				rs.close();
				stmt.close();
				conn.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			name.clear();
			businessName.clear();
			businessName.setDisable(true);
			contact.clear();
			phoneNumber.clear();
			email.clear();
			registration.setValue(LocalDate.now());
			webPage.clear();
			fax.clear();
			extraName1.clear();
			extraName2.clear();
			extraNumber1.clear();
			extraNumber2.clear();
			extraEmail1.clear();
			extraEmail2.clear();
			address.clear();
			town.clear();
			province.clear();
			shipmentAddress.clear();
			shipmentTown.clear();
			shipmentProvince.clear();
			CIF.clear();
			IVA.clear();
			IRPF.clear();
			bankAccount.clear();
			paymentMethod.clear();
			paymentMethodDescription.clear();
			paymentDeadline.clear();
			paymentDays.clear();
			profit.clear();
			discount.clear();
			budgetModel.clear();
			category.clear();
			description.clear();
			toggleName.setSelected(true);;
			checkboxAddress.setSelected(true);;
			checkboxExtraContact.setSelected(false);
			checkboxIncludeIVA.setSelected(false);
		}
	}
	  
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Circle clipPicture = new Circle(55, 55, 55);
		clientProfile.setClip(clipPicture);
				
		name.textProperty().addListener((obs2, oldName, newName) -> {
			if(toggleName.isSelected()) 
				businessName.setText(newName);
			nameLabel.setText(newName);});
		
		toggleName.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
			if(isNowSelected) 
				businessName.setText(name.getText());
			businessName.setDisable(isNowSelected);});
		
		description.textProperty().addListener((obs2, oldName, newName) -> 
			descriptionLabel.setText(newName));
		
		checkboxExtraContact.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
			if(isNowSelected) {
				extraContactContainer.setManaged(true);
				extraContactContainer.setVisible(true);
				checkboxExtraContact.setPadding(new Insets(0,0,15,0));
			} else {
				extraContactContainer.setManaged(false);
				extraContactContainer.setVisible(false);
				checkboxExtraContact.setPadding(new Insets(0,0,0,0));}});
		extraContactContainer.setManaged(false);
		extraContactContainer.setVisible(false);
		
		checkboxAddress.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
			if(!isNowSelected) {
				addressContainer.setManaged(true);
				addressContainer.setVisible(true);
			} else {
				addressContainer.setManaged(false);
				addressContainer.setVisible(false);}});
		addressContainer.setManaged(false);
		addressContainer.setVisible(false);
		alreadyExists.setManaged(false);
		alreadyExists.setVisible(false);

		String[] cities =  new String [SpanishCities.values().length];
		for(SpanishCities city: SpanishCities.values())
			cities[city.ordinal()] = city.getName();
		TextFields.bindAutoCompletion(province, cities);
	}
	
	@FXML
	private void saveClient() {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt;

			String sql = "SELECT name FROM clients WHERE name=?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, name.getText());
			ResultSet rs = stmt.executeQuery();
			
			if(!rs.isClosed() && !oldName.equalsIgnoreCase(name.getText()) || name.getText().isEmpty()) {
				name.getStyleClass().add("error");
				alreadyExists.setManaged(true);
				alreadyExists.setVisible(true);
			} else {
				name.getStyleClass().removeAll("error");
				alreadyExists.setManaged(false);
				alreadyExists.setVisible(false);
				
				if(oldName.isEmpty()) {
					sql = "INSERT INTO clients (name, address, "
						+ "town, province, phoneNumber, contact, registration, category, " 
						+ "businessName, email, fax, webPage, extraName1, extraNumber1, "
						+ "extraEmail1, extraName2, extraNumber2, "
						+ "extraEmail2, shipmentAddress, shipmentTown, shipmentProvince, "
						+ "CIF, IVA, IRPF, bankAccount, paymentMethod, paymentMethodDescription, "
						+ "paymentDeadline, paymentDays, profit, discount, budgetModel, description, "
						+ "toggleName, checkboxAddress, checkboxExtraContact, checkboxIncludeIVA)"
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
						+ "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				} else {
					sql = "UPDATE clients SET name=?, address=?, "
						+ "town=?, province=?, phoneNumber=?, contact=?, registration=?, category=?, " 
						+ "businessName=?, email=?, fax=?, webPage=?, extraName1=?, extraNumber1=?, "
						+ "extraEmail1=?, extraName2=?, extraNumber2=?, "
						+ "extraEmail2=?, shipmentAddress=?, shipmentTown=?, shipmentProvince=?, "
						+ "CIF=?, IVA=?, IRPF=?, bankAccount=?, paymentMethod=?, paymentMethodDescription=?, "
						+ "paymentDeadline=?, paymentDays=?, profit=?, discount=?, budgetModel=?, description=?, "
						+ "toggleName=?, checkboxAddress=?, checkboxExtraContact=?, checkboxIncludeIVA=? WHERE name=?";
				}
				stmt = conn.prepareStatement(sql);
				
				stmt.setString(1, name.getText());
				stmt.setString(2, address.getText());
				stmt.setString(3, town.getText());
				stmt.setString(4, province.getText());
				stmt.setString(5, phoneNumber.getText());
				stmt.setString(6, contact.getText());
				stmt.setString(7, registration.getValue().format(formatter));
				stmt.setString(8, category.getText());
				stmt.setString(9, businessName.getText());
				stmt.setString(10, email.getText());
				stmt.setString(11, fax.getText());
				stmt.setString(12, webPage.getText());
				stmt.setString(13, extraName1.getText());
				stmt.setString(14, extraNumber1.getText());
				stmt.setString(15, extraEmail1.getText());
				stmt.setString(16, extraName2.getText());
				stmt.setString(17, extraNumber2.getText());
				stmt.setString(18, extraEmail2.getText());
				stmt.setString(19, shipmentAddress.getText());
				stmt.setString(20, shipmentTown.getText());
				stmt.setString(21, shipmentProvince.getText());
				stmt.setString(22, CIF.getText());
				stmt.setString(23, IVA.getText());
				stmt.setString(24, IRPF.getText());
				stmt.setString(25, bankAccount.getText());
				stmt.setString(26, paymentMethod.getText());
				stmt.setString(27, paymentMethodDescription.getText());
				stmt.setString(28, paymentDeadline.getText());
				stmt.setString(29, paymentDays.getText());
				stmt.setString(30, profit.getText());
				stmt.setString(31, discount.getText());
				stmt.setString(32, budgetModel.getText());
				stmt.setString(33, description.getText());
				stmt.setBoolean(34, toggleName.isSelected());
				stmt.setBoolean(35, checkboxAddress.isSelected());
				stmt.setBoolean(36, checkboxExtraContact.isSelected());
				stmt.setBoolean(37, checkboxIncludeIVA.isSelected());
				if(!oldName.isEmpty())
					stmt.setString(38, oldName);
				
				stmt.execute();
				
				screenRequired("Clients");
			}
			rs.close();
			stmt.close();
			conn.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}