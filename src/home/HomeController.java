package home;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;

import files.ClientFileController;
import files.MachineFileController;
import files.PaperFileController;
import files.SupplierFileController;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import tables.ClientsController;
import tables.MachinesController;
import tables.PapersController;
import tables.SuppliersController;

public class HomeController implements Initializable {
	
	@FXML
	private VBox panel, screenContainer;
	@FXML
	private HBox toolbar;
    @FXML
    private ScrollPane scrollPane, sideMenu;
	@FXML
	private JFXButton clients, budgets, workOrders, bills, machines, preAndPostPrinting, papers,
	suppliers, userButton, notificationButton, backupButton, settingsButton, moreOptionsButton;
	@FXML
	private Label screenTitle, printerCoreLabel;
	@FXML
	private JFXHamburger hamburger;
	@FXML
	private Pane separator, toolbarSeparator;
	
	HamburgerBackArrowBasicTransition transition;
	
	private VBox clientsScreen, clientFile, suppliersScreen, supplierFile, papersScreen, paperFile, machinesScreen, machineFile;
	FXMLLoader clientFileLoader, supplierFileLoader, paperFileLoader, machineFileLoader;
	
    @Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		panel.setOnScroll(new EventHandler<ScrollEvent>() {
	        @Override
	        public void handle(ScrollEvent event) {
	            double deltaY = event.getDeltaY();
	            double width = scrollPane.getContent().getBoundsInLocal().getWidth();
	            double vvalue = scrollPane.getVvalue();
	            scrollPane.setVvalue(vvalue + -deltaY/width); // deltaY/width to make the scrolling equally fast regardless of the actual width of the component
	        }
	    });

		transition = new HamburgerBackArrowBasicTransition(hamburger);
		transition.setRate(1);
        transition.play();
		
        toolbar.getChildren().remove(moreOptionsButton);
        
		loadScreens();

		screenTitle.setText("Clientes");
		clients.getStyleClass().add("pressed");
		screenContainer.getChildren().add(clientsScreen);
		
		setListeners();
    }
	
    @FXML
    private void close() {
    	Stage stage = (Stage) screenContainer.getScene().getWindow();
    	stage.close();
    }
    
	@FXML
	private void changeScreen(ActionEvent event) {
		screenContainer.getChildren().clear();
		
		if(event.getSource() == clients) {
			screenTitle.setText("Clientes");
			unSelectButtons();
			clients.getStyleClass().add("pressed");
			
			screenContainer.getChildren().add(clientsScreen);

		} else if(event.getSource() == budgets) {
			screenTitle.setText("Presupuestos");
			unSelectButtons();
			budgets.getStyleClass().add("pressed");
			
		} else if(event.getSource() == workOrders) {
			screenTitle.setText("�rdenes de trabajo");
			unSelectButtons();
			workOrders.getStyleClass().add("pressed");
			
		} else if(event.getSource() == bills) {
			screenTitle.setText("Facturas");
			unSelectButtons();
			bills.getStyleClass().add("pressed");
			
		} else if(event.getSource() == machines) {
			screenTitle.setText("M�quinas de imprimir");
			unSelectButtons();
			machines.getStyleClass().add("pressed");
			
			screenContainer.getChildren().add(machinesScreen);
			
		} else if(event.getSource() == preAndPostPrinting) {
			screenTitle.setText("Pre y post Impresi�n");
			unSelectButtons();
			preAndPostPrinting.getStyleClass().add("pressed");
			
		} else if(event.getSource() == papers) {
			screenTitle.setText("Papeles");
			unSelectButtons();
			papers.getStyleClass().add("pressed");
			
			screenContainer.getChildren().add(papersScreen);
			
		} else if(event.getSource() == suppliers) {
			screenTitle.setText("Proveedores");
			unSelectButtons();
			suppliers.getStyleClass().add("pressed");
			
			screenContainer.getChildren().add(suppliersScreen);
		}
	}
	
	@FXML
	private void hideMenu() {
		if(screenContainer.getWidth() > 500 || (screenContainer.getWidth() <= 500 && sideMenu.getMaxWidth()!=70))
			if(sideMenu.getMaxWidth()!=70) {
				clients.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
				budgets.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
				workOrders.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
				bills.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
				machines.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
				preAndPostPrinting.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
				papers.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
				suppliers.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
				printerCoreLabel.setMaxWidth(0);
				HBox.setHgrow(separator, Priority.NEVER);
				
				Timeline timeline = new Timeline();
				timeline.getKeyFrames().add(new KeyFrame(Duration.millis(100), new KeyValue (sideMenu.maxWidthProperty(), 70, Interpolator.EASE_OUT)));
				timeline.play();
		        transition.setRate(transition.getRate()*-1);
		        transition.play();
				
			} else if(sideMenu.getMaxWidth()==70) {
				clients.setContentDisplay(ContentDisplay.LEFT);
				budgets.setContentDisplay(ContentDisplay.LEFT);
				workOrders.setContentDisplay(ContentDisplay.LEFT);
				bills.setContentDisplay(ContentDisplay.LEFT);
				machines.setContentDisplay(ContentDisplay.LEFT);
				preAndPostPrinting.setContentDisplay(ContentDisplay.LEFT);
				papers.setContentDisplay(ContentDisplay.LEFT);
				suppliers.setContentDisplay(ContentDisplay.LEFT);
				printerCoreLabel.setMaxWidth(-1);
				HBox.setHgrow(separator, Priority.ALWAYS);
				
				Timeline timeline = new Timeline();
				timeline.getKeyFrames().add(new KeyFrame(Duration.millis(100), new KeyValue (sideMenu.maxWidthProperty(), 237, Interpolator.EASE_OUT)));
				timeline.play();
		        transition.setRate(transition.getRate()*-1);
		        transition.play();
			}
	}
	
	private void toggleToolbar() {
		if(isToolbarCompacted()) {
			toolbar.getChildren().addAll(userButton, notificationButton, backupButton, settingsButton);
			toolbar.getChildren().remove(moreOptionsButton);
			
		} else {
			toolbar.getChildren().add(moreOptionsButton);
			toolbar.getChildren().removeAll(userButton, notificationButton, backupButton, settingsButton);
		}
	}
	
	private boolean isToolbarCompacted() {
		return toolbar.getChildren().contains(moreOptionsButton);
	}
	
	private void unSelectButtons() {
		clients.getStyleClass().removeAll("pressed");
		budgets.getStyleClass().removeAll("pressed");
		workOrders.getStyleClass().removeAll("pressed");
		bills.getStyleClass().removeAll("pressed");
		machines.getStyleClass().removeAll("pressed");
		preAndPostPrinting.getStyleClass().removeAll("pressed");
		papers.getStyleClass().removeAll("pressed");
		suppliers.getStyleClass().removeAll("pressed");
	}
	
	private void loadScreens() {
		try {
			clientsScreen = FXMLLoader.load(getClass().getResource("/tables/Clients.fxml"));
			clientFileLoader = new FXMLLoader(getClass().getResource("/files/ClientFile.fxml"));
			clientFile = clientFileLoader.load();
	
			suppliersScreen = FXMLLoader.load(getClass().getResource("/tables/Suppliers.fxml"));
			supplierFileLoader = new FXMLLoader(getClass().getResource("/files/SupplierFile.fxml"));
			supplierFile = supplierFileLoader.load();
			
			papersScreen = FXMLLoader.load(getClass().getResource("/tables/Papers.fxml"));
			paperFileLoader = new FXMLLoader(getClass().getResource("/files/PaperFile.fxml"));
			paperFile = paperFileLoader.load();
			
			machinesScreen = FXMLLoader.load(getClass().getResource("/tables/Machines.fxml"));
			machineFileLoader = new FXMLLoader(getClass().getResource("/files/MachineFile.fxml"));
			machineFile = machineFileLoader.load();
			
			VBox.setVgrow(clientsScreen, Priority.ALWAYS);
			VBox.setVgrow(clientFile, Priority.ALWAYS);
			VBox.setVgrow(suppliersScreen, Priority.ALWAYS);
			VBox.setVgrow(supplierFile, Priority.ALWAYS);
			VBox.setVgrow(papersScreen, Priority.ALWAYS);
			VBox.setVgrow(paperFile, Priority.ALWAYS);
			VBox.setVgrow(machinesScreen, Priority.ALWAYS);
			VBox.setVgrow(machineFile, Priority.ALWAYS);
						
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void setListeners() {

    	screenContainer.widthProperty().addListener(new ChangeListener<Number>() {
    	    @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth) {
    	    	if(newSceneWidth.intValue() < 500)
    	    			hideMenu();
    	    }
    	});
    	
    	toolbar.widthProperty().addListener(new ChangeListener<Number>() {
    	    @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth) {
    	    	if(newSceneWidth.intValue() < 450 && !isToolbarCompacted())
    	    		toggleToolbar();
    	    	else if (newSceneWidth.intValue() > 450 && isToolbarCompacted())
    	    		toggleToolbar();
    	    }
    	});
    	
		ClientsController.screenRequiredProperty().addListener((obs, oldScreenRequired, newScreenRequired) -> {
			if (newScreenRequired != "") ClientsController.screenRequiredProperty().set("");
			if (newScreenRequired == "NewClient") {
				screenTitle.setText("Nuevo cliente");
				screenContainer.getChildren().clear();
				screenContainer.getChildren().add(clientFile);
				clientFileLoader.<ClientFileController>getController().initData("");
				
			} else if (newScreenRequired == "Clients") {
				screenTitle.setText("Clientes");
				screenContainer.getChildren().clear();
				screenContainer.getChildren().add(clientsScreen);
				
			} else if (newScreenRequired != ""){
				screenTitle.setText("Cliente: "+newScreenRequired);
				screenContainer.getChildren().clear();
				screenContainer.getChildren().add(clientFile);
				clientFileLoader.<ClientFileController>getController().initData(newScreenRequired);
			}
		});

		SuppliersController.screenRequiredProperty().addListener((obs, oldScreenRequired, newScreenRequired) -> {
			if (newScreenRequired != "") SuppliersController.screenRequiredProperty().set("");
			if (newScreenRequired == "NewSupplier") {
				screenTitle.setText("Nuevo proveedor");
	        	screenContainer.getChildren().clear();
				screenContainer.getChildren().add(supplierFile);
				supplierFileLoader.<SupplierFileController>getController().initData("");
		        
	        } else if (newScreenRequired == "Suppliers") {
				screenTitle.setText("Proveedores");
				screenContainer.getChildren().clear();
				screenContainer.getChildren().add(suppliersScreen);
				
	        } else if (newScreenRequired != ""){
				screenTitle.setText("Proveedor: "+newScreenRequired);
	        	screenContainer.getChildren().clear();
				screenContainer.getChildren().add(supplierFile);
				supplierFileLoader.<SupplierFileController>getController().initData(newScreenRequired);
	        }
	    });
		
		PapersController.screenRequiredProperty().addListener((obs, oldScreenRequired, newScreenRequired) -> {
			if (newScreenRequired != "") PapersController.screenRequiredProperty().set("");
			if (newScreenRequired == "NewPaper") {
				screenTitle.setText("Nuevo Papel");
				screenContainer.getChildren().clear();
				screenContainer.getChildren().add(paperFile);
				paperFileLoader.<PaperFileController>getController().initData("");
				
			} else if (newScreenRequired == "Papers") {
				screenTitle.setText("Papeles");
				screenContainer.getChildren().clear();
				screenContainer.getChildren().add(papersScreen);
				
			} else if (newScreenRequired != ""){
				screenTitle.setText("Papel: "+newScreenRequired);
				screenContainer.getChildren().clear();
				screenContainer.getChildren().add(paperFile);
				paperFileLoader.<PaperFileController>getController().initData(newScreenRequired);
			}
		});

		MachinesController.screenRequiredProperty().addListener((obs, oldScreenRequired, newScreenRequired) -> {
			if (newScreenRequired != "") MachinesController.screenRequiredProperty().set("");
			if (newScreenRequired == "NewMachine") {
				screenTitle.setText("Nueva M�quina");
	        	screenContainer.getChildren().clear();
				screenContainer.getChildren().add(machineFile);
				machineFileLoader.<MachineFileController>getController().initData("");
		        
	        } else if (newScreenRequired == "Machines") {
				screenTitle.setText("M�quinas de imprimir");
				screenContainer.getChildren().clear();
				screenContainer.getChildren().add(papersScreen);
				
	        } else if (newScreenRequired != ""){
				screenTitle.setText("M�quina: "+newScreenRequired);
	        	screenContainer.getChildren().clear();
				screenContainer.getChildren().add(machineFile);
				machineFileLoader.<MachineFileController>getController().initData(newScreenRequired);
	        }
	    });
	}
}