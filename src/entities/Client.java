package entities;

public class Client {

	private int ID;
	private String name, address, town, province, phoneNumber, contact, registration, category;
	
	public Client() {
		
		ID = 0;
		name = "";
		address = "";
		town = "";
		province = "";
		phoneNumber = "";
		contact = "";
		registration = "";
		category = "";
	}
	
	public Client(int ID, String Name, String Address, String Town,
			String Province, String PhoneNumber, String Contact, String Registration,
			String Category) {
		
		this.ID = ID;
		this.name = Name;
		this.address = Address;
		this.town = Town;
		this.province = Province;
		this.phoneNumber = PhoneNumber;
		this.contact = Contact;
		this.registration = Registration;
		this.category = Category;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}