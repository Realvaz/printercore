package entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import utils.dbConnection;

public class Machine {


  	private int ID, printingBodies, beforeBodyNumber, averageSpeedField, minProductionTimeField,
		averageBootTime, averageTimePerPrinted, squareMetersPerTintKG, minConsumption;
  	
	private boolean radioboxAverageSpeed, checkboxMinProductionTime, radioboxTotalTime, radioboxTotalCost,
		allowsSheetFlip, radioboxAverageCost, checkboxMinProductionCost, includeIncreaseSheet,
		quadrichomiePrinted, numberedPrinted, flatBackgroundPrinted, specialQuality, machineProposal;	
	
	private String name, manufacturerAndModel, minDimensions, maxDimensions, colors,
		printerType, costsCalculationType, defaultTintHourProductionCost,
		hourPreparationCost, averagePlateCost, inputMargin, outputMargin, squareMargin,
		counterSquareMargin, averageCostField, minProductionCostField, averageBootCost, averageCostPerPrinted;
	
	private LocalDate costsDate;
	
	public Machine(String name, String manufacturerAndModel, String maxDimensions, String colors, LocalDate costsDate) {
		this.name = name;
		this.manufacturerAndModel = manufacturerAndModel;
		this.maxDimensions = maxDimensions;
		this.colors = colors;
		this.costsDate = costsDate;
	}

	public Machine(int ID, int printingBodies, int beforeBodyNumber, int averageSpeedField, int minProductionTimeField,
			int averageBootTime, int averageTimePerPrinted, int squareMetersPerTintKG, int minConsumption,
			boolean radioboxAverageSpeed, boolean checkboxMinProductionTime, boolean radioboxTotalTime,
			boolean radioboxTotalCost, boolean allowsSheetFlip, boolean radioboxAverageCost,
			boolean checkboxMinProductionCost, boolean includeIncreaseSheet, boolean quadrichomiePrinted,
			boolean numberedPrinted, boolean flatBackgroundPrinted, boolean specialQuality, boolean machineProposal,
			String name, String manufacturerAndModel, String minDimensions, String maxDimensions, String colors,
			String printerType, String costsCalculationType, String defaultTintHourProductionCost,
			String hourPreparationCost, String averagePlateCost, String inputMargin, String outputMargin,
			String squareMargin, String counterSquareMargin, String averageCostField, String minProductionCostField,
			String averageBootCost, String averageCostPerPrinted, LocalDate costsDate) {
		this.ID = ID;
		this.printingBodies = printingBodies;
		this.beforeBodyNumber = beforeBodyNumber;
		this.averageSpeedField = averageSpeedField;
		this.minProductionTimeField = minProductionTimeField;
		this.averageBootTime = averageBootTime;
		this.averageTimePerPrinted = averageTimePerPrinted;
		this.squareMetersPerTintKG = squareMetersPerTintKG;
		this.minConsumption = minConsumption;
		this.radioboxAverageSpeed = radioboxAverageSpeed;
		this.checkboxMinProductionTime = checkboxMinProductionTime;
		this.radioboxTotalTime = radioboxTotalTime;
		this.radioboxTotalCost = radioboxTotalCost;
		this.allowsSheetFlip = allowsSheetFlip;
		this.radioboxAverageCost = radioboxAverageCost;
		this.checkboxMinProductionCost = checkboxMinProductionCost;
		this.includeIncreaseSheet = includeIncreaseSheet;
		this.quadrichomiePrinted = quadrichomiePrinted;
		this.numberedPrinted = numberedPrinted;
		this.flatBackgroundPrinted = flatBackgroundPrinted;
		this.specialQuality = specialQuality;
		this.machineProposal = machineProposal;
		this.name = name;
		this.manufacturerAndModel = manufacturerAndModel;
		this.minDimensions = minDimensions;
		this.maxDimensions = maxDimensions;
		this.colors = colors;
		this.printerType = printerType;
		this.costsCalculationType = costsCalculationType;
		this.defaultTintHourProductionCost = defaultTintHourProductionCost;
		this.hourPreparationCost = hourPreparationCost;
		this.averagePlateCost = averagePlateCost;
		this.inputMargin = inputMargin;
		this.outputMargin = outputMargin;
		this.squareMargin = squareMargin;
		this.counterSquareMargin = counterSquareMargin;
		this.averageCostField = averageCostField;
		this.minProductionCostField = minProductionCostField;
		this.averageBootCost = averageBootCost;
		this.averageCostPerPrinted = averageCostPerPrinted;
		this.costsDate = costsDate;
	}
	
	public Machine(String name) {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM machines WHERE name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			
			ID = rs.getInt("ID");
			this.name = name;
			printingBodies = rs.getInt("printingBodies");
			beforeBodyNumber = rs.getInt("beforeBodyNumber");
			averageSpeedField = rs.getInt("averageSpeedField");
			minProductionTimeField = rs.getInt("minProductionTimeField");
			averageBootTime = rs.getInt("averageBootTime");
			averageTimePerPrinted = rs.getInt("averageTimePerPrinted");
			squareMetersPerTintKG = rs.getInt("squareMetersPerTintKG");
			minConsumption = rs.getInt("minConsumption");
			radioboxAverageSpeed = rs.getBoolean("radioboxAverageSpeed");
			checkboxMinProductionTime = rs.getBoolean("checkboxMinProductionTime");
			radioboxTotalTime = rs.getBoolean("radioboxTotalTime");
			radioboxTotalCost = rs.getBoolean("radioboxTotalCost");
			allowsSheetFlip = rs.getBoolean("allowsSheetFlip");
			radioboxAverageCost = rs.getBoolean("radioboxAverageCost");
			checkboxMinProductionCost = rs.getBoolean("checkboxMinProductionCost");
			includeIncreaseSheet = rs.getBoolean("includeIncreaseSheet");
			quadrichomiePrinted = rs.getBoolean("quadrichomiePrinted");
			numberedPrinted =rs.getBoolean("numberedPrinted");
			flatBackgroundPrinted = rs.getBoolean("flatBackgroundPrinted");
			specialQuality = rs.getBoolean("specialQuality");
			machineProposal = rs.getBoolean("machineProposal");
			manufacturerAndModel = rs.getString("manufacturerAndModel");
			minDimensions = rs.getString("minDimensions");
			maxDimensions = rs.getString("maxDimensions");
			colors = rs.getString("colors");
			printerType = rs.getString("printerType");
			costsCalculationType = rs.getString("costsCalculationType");
			defaultTintHourProductionCost = rs.getString("defaultTintHourProductionCost");
			hourPreparationCost = rs.getString("hourPreparationCost");
			averagePlateCost = rs.getString("averagePlateCost");
			inputMargin = rs.getString("inputMargin");
			outputMargin = rs.getString("outputMargin");
			squareMargin = rs.getString("squareMargin");
			counterSquareMargin = rs.getString("counterSquareMargin");
			averageCostField = rs.getString("averageCostField");
			minProductionCostField = rs.getString("minProductionCostField");
			averageBootCost = rs.getString("averageBootCost");
			averageCostPerPrinted = rs.getString("averageCostPerPrinted");
			costsDate = LocalDate.parse(rs.getString("costsDate"), DateTimeFormatter.ofPattern("dd/MM/yyyy"));

			rs.close();
			stmt.close();
			conn.close();
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertSupplier() {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO machines (printingBodies, "
				+ "beforeBodyNumber, averageSpeedField, minProductionTimeField, averageBootTime,"
				+ "averageTimePerPrinted, squareMetersPerTintKG, minConsumption, radioboxAverageSpeed,"
				+ "checkboxMinProductionTime, radioboxTotalTime, radioboxTotalCost, allowsSheetFlip,"
				+ "radioboxAverageCost, checkboxMinProductionCost, includeIncreaseSheet,"
				+ "quadrichomiePrinted, numberedPrinted, flatBackgroundPrinted, specialQuality,"
				+ "machineProposal, name, manufacturerAndModel, minDimensions, maxDimensions,"
				+ "colors, printerType, costsCalculationType, defaultTintHourProductionCost,"
				+ "hourPreparationCost, averagePlateCost, inputMargin, outputMargin,"
				+ "squareMargin, counterSquareMargin, averageCostField, minProductionCostField,"
				+ "averageBootCost, averageCostPerPrinted, costsDate)"
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
				+ "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setInt(1, printingBodies);
			stmt.setInt(2, beforeBodyNumber);
			stmt.setInt(3, averageSpeedField);
			stmt.setInt(4, minProductionTimeField);
			stmt.setInt(5, averageBootTime);
			stmt.setInt(6, averageTimePerPrinted);
			stmt.setInt(7, squareMetersPerTintKG);
			stmt.setInt(8, minConsumption);
			stmt.setBoolean(9, radioboxAverageSpeed);
			stmt.setBoolean(10, checkboxMinProductionTime);
			stmt.setBoolean(11, radioboxTotalTime);
			stmt.setBoolean(12, radioboxTotalCost);
			stmt.setBoolean(13, allowsSheetFlip);
			stmt.setBoolean(14, radioboxAverageCost);
			stmt.setBoolean(15, checkboxMinProductionCost);
			stmt.setBoolean(16, includeIncreaseSheet);
			stmt.setBoolean(17, quadrichomiePrinted);
			stmt.setBoolean(18, numberedPrinted);
			stmt.setBoolean(19, flatBackgroundPrinted);
			stmt.setBoolean(20, specialQuality);
			stmt.setBoolean(21, machineProposal);
			stmt.setString(22, name);
			stmt.setString(23, manufacturerAndModel);
			stmt.setString(24, minDimensions);
			stmt.setString(25, maxDimensions);
			stmt.setString(26, colors);
			stmt.setString(27, printerType);
			stmt.setString(28, costsCalculationType);
			stmt.setString(29, defaultTintHourProductionCost);
			stmt.setString(30, hourPreparationCost);
			stmt.setString(31, averagePlateCost);
			stmt.setString(32, inputMargin);
			stmt.setString(33, outputMargin);
			stmt.setString(34, squareMargin);
			stmt.setString(35, counterSquareMargin);
			stmt.setString(36, averageCostField);
			stmt.setString(37, minProductionCostField);
			stmt.setString(38, averageBootCost);
			stmt.setString(39, averageCostPerPrinted);
			stmt.setString(40, costsDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			
			stmt.execute();
			
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void updateSupplier() {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("UPDATE machines SET printingBodies=?,"
				+ "beforeBodyNumber=?, averageSpeedField=?, minProductionTimeField=?, averageBootTime=?,"
				+ "averageTimePerPrinted=?, squareMetersPerTintKG=?, minConsumption=?, radioboxAverageSpeed=?,"
				+ "checkboxMinProductionTime=?, radioboxTotalTime=?, radioboxTotalCost=?, allowsSheetFlip=?,"
				+ "radioboxAverageCost=?, checkboxMinProductionCost=?, includeIncreaseSheet=?,"
				+ "quadrichomiePrinted=?, numberedPrinted=?, flatBackgroundPrinted=?, specialQuality=?,"
				+ "machineProposal=?, name=?, manufacturerAndModel=?, minDimensions=?, maxDimensions=?,"
				+ "colors=?, printerType=?, costsCalculationType=?, defaultTintHourProductionCost=?,"
				+ "hourPreparationCost=?, averagePlateCost=?, inputMargin=?, outputMargin=?,"
				+ "squareMargin=?, counterSquareMargin=?, averageCostField=?, minProductionCostField=?,"
				+ "averageBootCost=?, averageCostPerPrinted=?, costsDate=? WHERE ID=?");
			stmt.setInt(1, printingBodies);
			stmt.setInt(2, beforeBodyNumber);
			stmt.setInt(3, averageSpeedField);
			stmt.setInt(4, minProductionTimeField);
			stmt.setInt(5, averageBootTime);
			stmt.setInt(6, averageTimePerPrinted);
			stmt.setInt(7, squareMetersPerTintKG);
			stmt.setInt(8, minConsumption);
			stmt.setBoolean(9, radioboxAverageSpeed);
			stmt.setBoolean(10, checkboxMinProductionTime);
			stmt.setBoolean(11, radioboxTotalTime);
			stmt.setBoolean(12, radioboxTotalCost);
			stmt.setBoolean(13, allowsSheetFlip);
			stmt.setBoolean(14, radioboxAverageCost);
			stmt.setBoolean(15, checkboxMinProductionCost);
			stmt.setBoolean(16, includeIncreaseSheet);
			stmt.setBoolean(17, quadrichomiePrinted);
			stmt.setBoolean(18, numberedPrinted);
			stmt.setBoolean(19, flatBackgroundPrinted);
			stmt.setBoolean(20, specialQuality);
			stmt.setBoolean(21, machineProposal);
			stmt.setString(22, name);
			stmt.setString(23, manufacturerAndModel);
			stmt.setString(24, minDimensions);
			stmt.setString(25, maxDimensions);
			stmt.setString(26, colors);
			stmt.setString(27, printerType);
			stmt.setString(28, costsCalculationType);
			stmt.setString(29, defaultTintHourProductionCost);
			stmt.setString(30, hourPreparationCost);
			stmt.setString(31, averagePlateCost);
			stmt.setString(32, inputMargin);
			stmt.setString(33, outputMargin);
			stmt.setString(34, squareMargin);
			stmt.setString(35, counterSquareMargin);
			stmt.setString(36, averageCostField);
			stmt.setString(37, minProductionCostField);
			stmt.setString(38, averageBootCost);
			stmt.setString(39, averageCostPerPrinted);
			stmt.setString(40, costsDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			stmt.setInt(41, ID);
			
			stmt.execute();
			
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean alreadyInDB() {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("SELECT name, ID FROM machines WHERE name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			
			Boolean existsInDB, itsNotItSelf;
			existsInDB = !rs.isClosed();
			
			if(existsInDB) itsNotItSelf = !(rs.getInt("ID") == ID);
			else itsNotItSelf = false;
			
			rs.close();
			stmt.close();
			conn.close();
			
			return (existsInDB && itsNotItSelf);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public static List<String> getAll() {
		try {
			Connection conn = dbConnection.getConnection();
			ResultSet rs = conn.createStatement().executeQuery("SELECT name FROM machines");

			List<String> list = new ArrayList<String>();
			while(rs.next()) list.add(rs.getString("name"));
			
			rs.close();
			conn.close();
			
			return list;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int getID() {
		return ID;
	}

	public int getPrintingBodies() {
		return printingBodies;
	}

	public int getBeforeBodyNumber() {
		return beforeBodyNumber;
	}

	public int getAverageSpeedField() {
		return averageSpeedField;
	}

	public int getMinProductionTimeField() {
		return minProductionTimeField;
	}

	public int getAverageBootTime() {
		return averageBootTime;
	}

	public int getAverageTimePerPrinted() {
		return averageTimePerPrinted;
	}

	public int getSquareMetersPerTintKG() {
		return squareMetersPerTintKG;
	}

	public int getMinConsumption() {
		return minConsumption;
	}

	public boolean isRadioboxAverageSpeed() {
		return radioboxAverageSpeed;
	}

	public boolean isCheckboxMinProductionTime() {
		return checkboxMinProductionTime;
	}

	public boolean isRadioboxTotalTime() {
		return radioboxTotalTime;
	}

	public boolean isRadioboxTotalCost() {
		return radioboxTotalCost;
	}

	public boolean isAllowsSheetFlip() {
		return allowsSheetFlip;
	}

	public boolean isRadioboxAverageCost() {
		return radioboxAverageCost;
	}

	public boolean isCheckboxMinProductionCost() {
		return checkboxMinProductionCost;
	}

	public boolean isIncludeIncreaseSheet() {
		return includeIncreaseSheet;
	}

	public boolean isQuadrichomiePrinted() {
		return quadrichomiePrinted;
	}

	public boolean isNumberedPrinted() {
		return numberedPrinted;
	}

	public boolean isFlatBackgroundPrinted() {
		return flatBackgroundPrinted;
	}

	public boolean isSpecialQuality() {
		return specialQuality;
	}

	public boolean isMachineProposal() {
		return machineProposal;
	}

	public String getName() {
		return name;
	}

	public String getManufacturerAndModel() {
		return manufacturerAndModel;
	}

	public String getMinDimensions() {
		return minDimensions;
	}

	public String getMaxDimensions() {
		return maxDimensions;
	}

	public String getColors() {
		return colors;
	}

	public String getPrinterType() {
		return printerType;
	}

	public String getCostsCalculationType() {
		return costsCalculationType;
	}

	public String getDefaultTintHourProductionCost() {
		return defaultTintHourProductionCost;
	}

	public String getHourPreparationCost() {
		return hourPreparationCost;
	}

	public String getAveragePlateCost() {
		return averagePlateCost;
	}

	public String getInputMargin() {
		return inputMargin;
	}

	public String getOutputMargin() {
		return outputMargin;
	}

	public String getSquareMargin() {
		return squareMargin;
	}

	public String getCounterSquareMargin() {
		return counterSquareMargin;
	}

	public String getAverageCostField() {
		return averageCostField;
	}

	public String getMinProductionCostField() {
		return minProductionCostField;
	}

	public String getAverageBootCost() {
		return averageBootCost;
	}

	public String getAverageCostPerPrinted() {
		return averageCostPerPrinted;
	}

	public LocalDate getCostsDate() {
		return costsDate;
	}
}