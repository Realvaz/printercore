package entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import utils.dbConnection;

public class Paper {

	private int ID;
	private String name, supplier, paperType, grammage, dimensions, unit,
			unitaryCost, description, storeLocation, lot, reference, extraInfo;
	boolean reelPrint, unitPrint, sheetPrint;
	LocalDate costsDate;
	
	public Paper(int ID, String name, String supplier, String paperType, String grammage, String dimensions,
			String unit, String unitaryCost, boolean sheetPrint, boolean unitPrint, boolean reelPrint, String description,
			LocalDate costsDate, String storeLocation, String lot, String reference, String extraInfo) {
		this.ID = ID;
		this.name = name;
		this.supplier = supplier;
		this.paperType = paperType;
		this.grammage = grammage;
		this.dimensions = dimensions;
		this.unit = unit;
		this.unitaryCost = unitaryCost;
		this.sheetPrint = sheetPrint;
		this.unitPrint = unitPrint;
		this.reelPrint = reelPrint;
		this.description = description;
		this.costsDate = costsDate;
		this.storeLocation = storeLocation;
		this.lot = lot;
		this.reference = reference;
		this.extraInfo = extraInfo;
	}

	public Paper(String name, String supplier, String paperType, String grammage, String dimensions, String unit, String unitaryCost) {
		this.name = name;
		this.supplier = supplier;
		this.paperType = paperType;
		this.grammage = grammage;
		this.dimensions = dimensions;
		this.unit = unit;
		this.unitaryCost = unitaryCost;
	}
	
	public Paper(String name) {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM papers WHERE name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			
			ID = rs.getInt("ID");
			this.name = name;
			supplier = rs.getString("supplier");
			paperType = rs.getString("paperType");
			grammage = rs.getString("grammage");
			dimensions = rs.getString("dimensions");
			unit = rs.getString("unit");
			unitaryCost = rs.getString("unitaryCost");
			sheetPrint = rs.getBoolean("sheetPrint");
			unitPrint = rs.getBoolean("unitPrint");
			reelPrint = rs.getBoolean("reelPrint");
			description = rs.getString("description");
			costsDate = LocalDate.parse(rs.getString("costsDate"), DateTimeFormatter.ofPattern("dd/MM/yyyy"));;
			storeLocation = rs.getString("storeLocation");
			lot = rs.getString("lot");
			reference = rs.getString("reference");
			extraInfo = rs.getString("extraInfo");

			rs.close();
			stmt.close();
			conn.close();
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insert() {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO papers (name, "
				+ "supplier, paperType, grammage, dimensions, unit, unitaryCost, " 
				+ "sheetPrint, unitPrint, reelPrint, description, costsDate, storeLocation, "
				+ "lot, reference, extraInfo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setString(1, name);
			stmt.setString(2, supplier);
			stmt.setString(3, paperType);
			stmt.setString(4, grammage);
			stmt.setString(5, dimensions);
			stmt.setString(6, unit);
			stmt.setString(7, unitaryCost);
			stmt.setBoolean(8, sheetPrint);
			stmt.setBoolean(9, unitPrint);
			stmt.setBoolean(10, reelPrint);
			stmt.setString(11, description);
			stmt.setString(12, costsDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			stmt.setString(13, storeLocation);
			stmt.setString(14, lot);
			stmt.setString(15, reference);
			stmt.setString(16, extraInfo);
			
			stmt.execute();
			
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void update() {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("UPDATE papers SET name=?, "
				+ "supplier=?, paperType=?, grammage=?, dimensions=?, unit=?, unitaryCost=?, " 
				+ "sheetPrint=?, unitPrint=?, reelPrint=?, description=?, costsDate=?, storeLocation=?, "
				+ "lot=?, reference=?, extraInfo=? WHERE ID=?");
			stmt.setString(1, name);
			stmt.setString(2, supplier);
			stmt.setString(3, paperType);
			stmt.setString(4, grammage);
			stmt.setString(5, dimensions);
			stmt.setString(6, unit);
			stmt.setString(7, unitaryCost);
			stmt.setBoolean(8, sheetPrint);
			stmt.setBoolean(9, unitPrint);
			stmt.setBoolean(10, reelPrint);
			stmt.setString(11, description);
			stmt.setString(12, costsDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			stmt.setString(13, storeLocation);
			stmt.setString(14, lot);
			stmt.setString(15, reference);
			stmt.setString(16, extraInfo);
			stmt.setInt(17, ID);
			
			stmt.execute();
			
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean alreadyInDB() {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("SELECT name, ID FROM papers WHERE name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			
			Boolean existsInDB, itsNotItSelf;
			existsInDB = !rs.isClosed();
			
			if(existsInDB) itsNotItSelf = !(rs.getInt("ID") == ID);
			else itsNotItSelf = false;
			
			rs.close();
			stmt.close();
			conn.close();
			
			return (existsInDB && itsNotItSelf);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public static List<String> getAll() {
		try {
			Connection conn = dbConnection.getConnection();
			ResultSet rs = conn.createStatement().executeQuery("SELECT name FROM papers");

			List<String> list = new ArrayList<String>();
			while(rs.next()) list.add(rs.getString("name"));
			
			rs.close();
			conn.close();
			
			return list;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public int getID() {
		return ID;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSupplier() {
		return supplier;
	}
	
	public String getPaperType() {
		return paperType;
	}
	
	public String getGrammage() {
		return grammage;
	}
	
	public String getDimensions() {
		return dimensions;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public String getUnitaryCost() {
		return unitaryCost;
	}
	
	public boolean getSheetPrint() {
		return sheetPrint;
	}
	
	public boolean getUnitPrint() {
		return unitPrint;
	}
	
	public boolean getReelPrint() {
		return reelPrint;
	}
	
	public String getDescription() {
		return description;
	}
	
	public LocalDate getCostsDate() {
		return costsDate;
	}
	
	public String getStoreLocation() {
		return storeLocation;
	}
	
	public String getLot() {
		return lot;
	}
	
	public String getReference() {
		return reference;
	}
	
	public String getExtraInfo() {
		return extraInfo;
	}
}