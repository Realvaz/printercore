package entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import utils.dbConnection;

public class Supplier {

	private int ID;
	private String name, town, province, contact, phoneNumber, category,
					businessName, fax, email, webPage, extraName1, extraEmail1, extraNumber1,
					extraName2, extraEmail2, extraNumber2, address, description;
	private LocalDate registration;
	private Boolean toggleName;
	
	public Supplier(int ID, String name, String town, String province, String contact, String phoneNumber,
			String category, String businessName, String fax, String email, String webPage, String extraName1,
			String extraEmail1, String extraNumber1, String extraName2, String extraEmail2, String extraNumber2,
			String address, String description, LocalDate registration, Boolean toggleName) {
		this.ID = ID;
		this.name = name;
		this.town = town;
		this.province = province;
		this.contact = contact;
		this.phoneNumber = phoneNumber;
		this.category = category;
		this.businessName = businessName;
		this.fax = fax;
		this.email = email;
		this.webPage = webPage;
		this.extraName1 = extraName1;
		this.extraEmail1 = extraEmail1;
		this.extraNumber1 = extraNumber1;
		this.extraName2 = extraName2;
		this.extraEmail2 = extraEmail2;
		this.extraNumber2 = extraNumber2;
		this.address = address;
		this.description = description;
		this.registration = registration;
		this.toggleName = toggleName;
	}
	
	public Supplier(int ID, String name, String town, String province, String contact, String phoneNumber,
			LocalDate registration, String category) {
		this.ID = ID;
		this.name = name;
		this.town = town;
		this.province = province;
		this.contact = contact;
		this.phoneNumber = phoneNumber;
		this.registration = registration;
		this.category = category;
	}
	
	public Supplier(String name) {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("SELECT * FROM suppliers WHERE name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			
			ID = rs.getInt("ID");
			this.name = name;
			town = rs.getString("town");
			province = rs.getString("province");
			contact = rs.getString("contact");
			phoneNumber = rs.getString("phoneNumber");
			category = rs.getString("category");
			businessName = rs.getString("businessName");
			fax = rs.getString("fax");
			email = rs.getString("email");
			webPage = rs.getString("webPage");
			extraName1 = rs.getString("extraName1");
			extraEmail1 = rs.getString("extraEmail1");
			extraNumber1 = rs.getString("extraNumber1");
			extraName2 = rs.getString("extraName2");
			extraEmail2 = rs.getString("extraEmail2");
			extraNumber2 = rs.getString("extraNumber2");
			address = rs.getString("address");
			description = rs.getString("description");
			registration = LocalDate.parse(rs.getString("registration"), DateTimeFormatter.ofPattern("dd/MM/yyyy"));

			rs.close();
			stmt.close();
			conn.close();
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertSupplier() {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO suppliers (name, "
				+ "town, province, phoneNumber, contact, registration, category, " 
				+ "businessName, email, fax, webPage, extraName1, extraNumber1, "
				+ "extraEmail1, extraName2, extraNumber2, extraEmail2, address, description, toggleName)"
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			stmt.setString(1, name);
			stmt.setString(2, town);
			stmt.setString(3, province);
			stmt.setString(4, phoneNumber);
			stmt.setString(5, contact);
			stmt.setString(6, registration.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			stmt.setString(7, category);
			stmt.setString(8, businessName);
			stmt.setString(9, email);
			stmt.setString(10, fax);
			stmt.setString(11, webPage);
			stmt.setString(12, extraName1);
			stmt.setString(13, extraNumber1);
			stmt.setString(14, extraEmail1);
			stmt.setString(15, extraName2);
			stmt.setString(16, extraNumber2);
			stmt.setString(17, extraEmail2);
			stmt.setString(18, address);
			stmt.setString(19, description);
			stmt.setBoolean(20, toggleName);
			
			stmt.execute();
			
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void updateSupplier() {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("UPDATE suppliers SET name=?, town=?, province=?, phoneNumber=?,"
					+ "contact=?, registration=?, category=?, businessName=?, email=?, fax=?,"
					+ "webPage=?, extraName1=?, extraNumber1=?, extraEmail1=?, extraName2=?,"
					+ "extraNumber2=?, extraEmail2=?, address=?, description=?, toggleName=? WHERE ID=?");
			stmt.setString(1, name);
			stmt.setString(2, town);
			stmt.setString(3, province);
			stmt.setString(4, phoneNumber);
			stmt.setString(5, contact);
			stmt.setString(6, registration.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			stmt.setString(7, category);
			stmt.setString(8, businessName);
			stmt.setString(9, email);
			stmt.setString(10, fax);
			stmt.setString(11, webPage);
			stmt.setString(12, extraName1);
			stmt.setString(13, extraNumber1);
			stmt.setString(14, extraEmail1);
			stmt.setString(15, extraName2);
			stmt.setString(16, extraNumber2);
			stmt.setString(17, extraEmail2);
			stmt.setString(18, address);
			stmt.setString(19, description);
			stmt.setBoolean(20, toggleName);
			stmt.setInt(21, ID);
			
			stmt.execute();
			
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean alreadyInDB() {
		try {
			Connection conn = dbConnection.getConnection();
			PreparedStatement stmt = conn.prepareStatement("SELECT name, ID FROM suppliers WHERE name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			
			Boolean existsInDB, itsNotItSelf;
			existsInDB = !rs.isClosed();
			
			if(existsInDB) itsNotItSelf = !(rs.getInt("ID") == ID);
			else itsNotItSelf = false;
			
			rs.close();
			stmt.close();
			conn.close();
			
			return (existsInDB && itsNotItSelf);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public static List<String> getAll() {
		try {
			Connection conn = dbConnection.getConnection();
			ResultSet rs = conn.createStatement().executeQuery("SELECT name FROM suppliers");

			List<String> list = new ArrayList<String>();
			while(rs.next()) list.add(rs.getString("name"));
			
			rs.close();
			conn.close();
			
			return list;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public int getID() {
		return ID;
	}

	public String getName() {
		return name;
	}

	public String getTown() {
		return town;
	}

	public String getProvince() {
		return province;
	}

	public String getContact() {
		return contact;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public String getCategory() {
		return category;
	}

	public String getBusinessName() {
		return businessName;
	}

	public String getFax() {
		return fax;
	}
	public String getEmail() {
		return email;
	}

	public String getWebPage() {
		return webPage;
	}

	public String getExtraName1() {
		return extraName1;
	}

	public String getExtraEmail1() {
		return extraEmail1;
	}

	public String getExtraNumber1() {
		return extraNumber1;
	}

	public String getExtraName2() {
		return extraName2;
	}

	public String getExtraEmail2() {
		return extraEmail2;
	}

	public String getExtraNumber2() {
		return extraNumber2;
	}

	public String getAddress() {
		return address;
	}

	public String getDescription() {
		return description;
	}

	public LocalDate getRegistration() {
		return registration;
	}
	
	public Boolean getToggleName() {
		return toggleName;
	}
}